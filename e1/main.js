"use strict"

// const log=R.bind(console.log, console)

//x:Number -> y:Number -> {x:Number, y:Number}:Object
const vec = R.useWith(R.merge, [R.objOf("x"), R.objOf("y")])

const applyFnToVec = (fn, angle, length)=> R.multiply(fn(angle), length)

const toVec = (angle, length)=>({
    x: applyFnToVec(Math.cos, angle, length),
    y: applyFnToVec(Math.sin, angle, length)
})
const center = ({width, height})=> vec(width / 2, height / 2)
const _center = center
const subtract = (v1, v2)=> vec(v1.x - v2.x, v1.y - v2.y)
const add = (v1, v2)=> vec(v1.x + v2.x, v1.y + v2.y)
const multiply = ({x, y}, scalar)=> vec(x * scalar, y * scalar)
const topLeft = ({position, dimension})=> subtract(position, center(dimension))
const dimension = (width, height) => ({width, height})
const radian = (degree)=> (Math.PI / 180) * degree
const random = ({base, offset}) => Math.random() * offset + base
const mapWithIndex = R.addIndex(R.map)
const updateParticle = (particle, c) => {
    const {position, velocity} = particle
    const merge = R.merge(particle, {position: add(position, velocity)});

    if (c.isVecOutOfBounds(particle.position)) {
        return R.merge(particle, {position: c.center()});
    }

    return merge
}

const Vector = (({x, y})=> {
    const toString = R.memoize(() => `{x:${x},y:${y}}`)
    const angle = R.memoize(()=> Math.atan2(y, x))
    const length = R.memoize(()=> Math.sqrt(x * x + y * y))
    return {
        x: () => x,
        y: () => y,
        toString,
        s: toString,
        angle,
        length
    }
})

Vector.fromXY = (x, y)=>Vector({x, y})
Vector.fromAngleLength = R.compose(Vector, toVec)
Vector.fromAL = Vector.fromAngleLength

const vector12 = Vector.fromXY(1, 2)
// console.log(vector12.toString())
console.assert(vector12.x() === 1)
console.assert(vector12.y() === 2)

const vectorAL12 = Vector.fromAngleLength(1, 2)
// console.log(vectorAL12.toString())
console.assert(vectorAL12.x() === 1.0806046117362795)
console.assert(vectorAL12.y() === 1.682941969615793)


const Canvas = (canvas)=> {
    const context = canvas.getContext("2d")
    const dimension = ()=> R.pick(["width", "height"], canvas)
    const resizeToWindow = () => {
        canvas.width = window.innerWidth
        canvas.height = window.innerHeight
    }
    const isVecOutOfBounds = ({x, y}) => (x < 0 || x > canvas.width || y < 0 || y > canvas.height)

    const center = () => _center(dimension())

    const resizeToWindowOnWindowResize = () => {
        window.addEventListener("resize", () => resizeToWindow(canvas))
        resizeToWindow(canvas)
    }

    return R.merge(
        {resizeToWindowOnWindowResize, dimension, isVecOutOfBounds, center},
        bindAndGetAllFunctionsInPrototypeChain(context))
}
Canvas.fromElementID = R.compose(
    Canvas, R.tap(clog),
    R.bind(document.getElementById, document)
)


window.onload = function () {
    const c = Canvas.fromElementID("canvas")
    c.resizeToWindowOnWindowResize()

    const canvas = document.getElementById("canvas"),
        context = canvas.getContext("2d")

    const boundContextFunctions = bindAndGetAllFunctionsInPrototypeChain(context)
    const options = {context, boundContextFunctions, c}

    const {createRandomState} = NS.stateHelperModule()
    let initialState = createRandomState({canvasDimension: R.pick(["width", "height"], canvas)})

    const rafStream = Kefir.stream(emitter => {
        const callback = (totalElaspsedTime) => {
            emitter.emit(totalElaspsedTime)
            requestAnimationFrame(callback)
        }
        requestAnimationFrame(callback)
    })

    rafStream
        .diff(R.flip(R.subtract))
        .map(R.divide(R.__, 1000))
        .scan(
            (state, elapsedSeconds)=> {
                const newOptions = R.merge(options, {elapsedSeconds, state})
                NS.renderHelperModule().render(newOptions, state)
                return NS.stateHelperModule().update(newOptions, state)
            },
            initialState
        )
        .onValue((state)=> {

        })
}

const NS = {
    renderHelperModule(){
        const publicAPI = {
            contextHelper: (c) => {
                const moveTo = (p)=> {
                    c.moveTo(p.x | 0, p.y | 0)
                }
                const lineTo = (p)=> {
                    c.lineTo(p.x | 0, p.y | 0)
                }
                const line = ([p1,p2])=> {
                    moveTo(p1)
                    lineTo(p2)
                }
                const translate = ({x, y})=> {
                    c.translate(x, y)
                }
                return {moveTo, lineTo, line, translate}
            },
            render: (options, state) => {
                const {boundContextFunctions, context:{canvas:{width, height}}, context} = options
                const {clearRect, beginPath, stroke} = boundContextFunctions
                const {moveTo, lineTo, line, translate} = publicAPI.contextHelper(boundContextFunctions)

                clearRect(0, 0, width, height)

                context.strokeStyle = 'rgba(0, 0, 0, 0.2)'
                beginPath()
                R.forEach(line, state.lines)
                stroke()


                function renderParticle(particle) {
                    // R.forEach(line, particle.lines)
                    const {x, y} = particle.position
                    beginPath()
                    context.arc(x, y, 10, radian(0), radian(360), false)
                    stroke()

                }

                context.strokeStyle = 'rgba(256, 256, 256, 0.8)'
                beginPath()
                R.map(renderParticle, state.particles)
                stroke()


                // renderParticle(state.hero)

                // context.save()
                // context.translate(0, height / 2)
                // context.scale(1, -1)
                // for(var angle = 0 angle < Math.PI * 2 angle += .01) {
                //     const angleMultiplier = 100
                //     var x = angle * angleMultiplier,
                //         y = Math.sin(angle) * angleMultiplier
                //
                //     context.fillStyle = "blue"
                //     const rectSide = 10
                //     context.fillRect(x, y, rectSide, rectSide)
                //
                //     y = Math.cos(angle) * angleMultiplier
                //     context.fillStyle = "green"
                //     context.fillRect(x, y, rectSide, rectSide)
                // }
                // context.restore()

                context.save()
                var centerY = height * .5,
                    centerX = width * .5,
                    baseAlpha = 0.4,
                    offset = 0.4,
                    angle = state.ep3.angle
                context.translate(centerX, centerY)
                var alpha = baseAlpha + Math.cos(angle) * offset
                // context.fillStyle = "rgba(0, 0, 256, " + alpha + ")"
                context.beginPath()
                const radius = 100
                context.arc(0, 0, radius, 0, Math.PI * 2, false)
                context.clip()
                context.translate(-radius, -radius)
                context.beginPath()
                R.forEach(line, state.ep3.lines)
                context.strokeStyle = 'rgba(0, 256, 256, ' + alpha + ')'
                stroke()
                context.restore()
            }
        }
        return publicAPI
    },
    stateHelperModule(){
        const randomPoint = ({width, height} = {width: 1, height: 1}) => {
            return vec(Math.random() * width, Math.random() * height)
            // return {x: (Math.random() * width), y: (Math.random() * height)}
        }

        const randomPointGenerator = (dimension)=> ()=>randomPoint(dimension);

        const randomPointPair = R.compose(R.times(R.__, 2), randomPointGenerator)

        const randomPointPairGenerator = (dimension)=> ()=>randomPointPair(dimension);

        const randomLines = ({dimension, totalPairs}) =>
            R.times(randomPointPairGenerator(dimension), totalPairs)

        // const randomLines = ({dimension, totalPairs}) => R.times(()=>randomPointPair(dimension), totalPairs)

        const publicAPI = {
            createRandomState({canvasDimension}){
                const randomPointPairCanvasBound = R.partial(randomPointPair, [canvasDimension])

                const createParticle = () => {
                    var width = random({base: 10, offset: 0})
                    const squareDimension = R.useWith(dimension, [R.__, R.__])
                    const particleDimension = squareDimension(width)
                    return {
                        lines: randomLines({dimension: particleDimension, totalPairs: 100}),
                        dimension: particleDimension,
                        position: center(canvasDimension),
                        velocity: toVec(Math.random() * radian(360), 2)
                    }
                }

                const hero = createParticle()
                return {
                    lines: R.times(randomPointPairCanvasBound, 10000),
                    hero,
                    particles: R.times(createParticle, 1000),
                    ep3: {
                        angle: 0,
                        lines: randomLines({dimension: {width: 200, height: 200}, totalPairs: 500})
                    }
                }
            },
            update({context:{canvas}, elapsedSeconds, c}, state){
                state.ep3.angle += (1 * elapsedSeconds)

                function positionPath(pathPrefix) {
                    return R.concat(pathPrefix, ["position"])
                }

                function updateParticleReducer({position, velocity}, pathPrefix = []) {
                    return R.assocPath(positionPath(pathPrefix), add(position, multiply(velocity, elapsedSeconds)))
                }


                const particles = R.map((p)=>updateParticle(p, c), state.particles)

                // const p = particles[0];
                //
                // if (c.isVecOutOfBounds(p.position)) {
                //     p.position = c.center()
                // }

                return R.compose(
                    R.assocPath(["particles"], particles),
                    updateParticleReducer(state.hero, ["hero"])
                )
                (state)
                return state

                return publicAPI.createRandomState({canvasDimension: R.pick(["width", "height"], canvas)})
            }
        }
        return publicAPI
    }
}

