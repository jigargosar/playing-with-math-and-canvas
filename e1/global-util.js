const bindOwnFn = (object, ownFunctionName) => R.bind(object[ownFunctionName], object)
const getPrototypeOf = bindOwnFn(Object, "getPrototypeOf")
const getOwnPropertyNames = bindOwnFn(Object, "getOwnPropertyNames")
const clog = bindOwnFn(console, "log")

function bindAndGetAllFunctionsInPrototypeChain(object) {

    var prototypeChain = (object) =>
        R.ifElse(
            R.compose(R.isNil, getPrototypeOf),
            R.F,
            R.compose(R.pair(object, R.__), getPrototypeOf))
        (object)

    return R.compose(
        // R.tap(log),
        R.map(R.bind(R.__, object)),
        // R.tap(log),
        R.pickBy(R.is(Function)),
        R.pick(R.__, object),
        R.uniq,
        R.reject(R.equals("webkitImageSmoothingEnabled")), //to remove deprecation warning in logs
        R.chain(getOwnPropertyNames),
        R.unfold(prototypeChain))
    (object)
}
